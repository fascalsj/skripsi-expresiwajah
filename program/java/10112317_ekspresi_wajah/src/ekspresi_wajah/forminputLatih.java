package ekspresi_wajah;

import static ekspresi_wajah.fungsi.*;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.objdetect.CascadeClassifier;
import Jama.Matrix;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

public class forminputLatih extends javax.swing.JFrame {

    String dirWajah = "lib/haarcascade_frontalface_alt.xml";
    String direktoriBahagia = "dataLatihGambar/Bahagia";
    String direktoriMarah = "dataLatihGambar/Marah";
    String direktoriMuak = "dataLatihGambar/Muak";
    String direktoriSedih = "dataLatihGambar/Sedih";
    String direktoriTakut = "dataLatihGambar/Takut";
    String direktoriTerkejut = "dataLatihGambar/Terkejut";
    String tempImage = "";
    BufferedImage imageAll;
    BufferedImage faceImage;
    int codeBlockSize = 33;
    int vectorHeight = 4;
    int vectorWidth = 4;
    int filterBlok = 3;
    int bitMatriks = 32;
    String labelLatih = "1";
    int imageWidth;
    int imageHeight;
//    Mat matWajah;
    BufferedImage ikonAsli = null;
    BufferedImage ikonGrayscale = null;
    BufferedImage ikonBlur = null;
    BufferedImage ikonVQ = null;

    public forminputLatih() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnBrowseImage = new javax.swing.JButton();
        setGambar = new javax.swing.JLabel();
        labelNama = new javax.swing.JLabel();
        btnClose = new javax.swing.JButton();
        buttonEsktrak = new javax.swing.JButton();
        btnLatihMasal = new javax.swing.JButton();
        setlDeteksiObjekWajah = new javax.swing.JLabel();
        labelNama2 = new javax.swing.JLabel();
        lowpassFilter = new javax.swing.JButton();
        labelNama8 = new javax.swing.JLabel();
        setLowpass = new javax.swing.JLabel();
        buttonEsktrak4 = new javax.swing.JButton();
        labelNama11 = new javax.swing.JLabel();
        setVQ = new javax.swing.JLabel();
        buttonEsktrak5 = new javax.swing.JButton();
        jfilterreg = new javax.swing.JComboBox<>();
        jcomlabel = new javax.swing.JComboBox<>();
        labelNama12 = new javax.swing.JLabel();
        labelNama13 = new javax.swing.JLabel();
        labelNama14 = new javax.swing.JLabel();
        labelNama1 = new javax.swing.JLabel();
        DeteksiObjek = new javax.swing.JButton();
        btnTampilCitra = new javax.swing.JButton();
        buttonEsktrak6 = new javax.swing.JButton();
        CitraDeteksi = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Menu Latih");
        setPreferredSize(new java.awt.Dimension(1300, 400));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnBrowseImage.setText("Browse");
        btnBrowseImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseImageActionPerformed(evt);
            }
        });
        getContentPane().add(btnBrowseImage, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 20, -1, -1));

        setGambar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getContentPane().add(setGambar, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 200, 200));

        labelNama.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
        labelNama.setText(".");
        getContentPane().add(labelNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 250, -1, -1));

        btnClose.setBackground(new java.awt.Color(255, 0, 0));
        btnClose.setText("Menu Utama");
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        getContentPane().add(btnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 10, -1, -1));

        buttonEsktrak.setText("Input Data Latih");
        buttonEsktrak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEsktrakActionPerformed(evt);
            }
        });
        getContentPane().add(buttonEsktrak, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 150, -1, 40));

        btnLatihMasal.setText(".");
        btnLatihMasal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLatihMasalActionPerformed(evt);
            }
        });
        getContentPane().add(btnLatihMasal, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 320, 10, 10));

        setlDeteksiObjekWajah.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getContentPane().add(setlDeteksiObjekWajah, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 50, 200, 200));

        labelNama2.setText("Citra Wajah");
        getContentPane().add(labelNama2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 30, -1, -1));

        lowpassFilter.setText("Lowpass Filter");
        lowpassFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lowpassFilterActionPerformed(evt);
            }
        });
        getContentPane().add(lowpassFilter, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 260, -1, -1));

        labelNama8.setText("Citra Wajah Lowpass");
        getContentPane().add(labelNama8, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 30, -1, -1));

        setLowpass.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getContentPane().add(setLowpass, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 50, 200, 200));

        buttonEsktrak4.setText("Vector Quantization");
        buttonEsktrak4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEsktrak4ActionPerformed(evt);
            }
        });
        getContentPane().add(buttonEsktrak4, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 260, -1, -1));

        labelNama11.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
        labelNama11.setText(".");
        getContentPane().add(labelNama11, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 120, 20, 20));

        setVQ.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getContentPane().add(setVQ, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 50, 200, 200));

        buttonEsktrak5.setText("Tampil Nilai Vector Quantization");
        buttonEsktrak5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEsktrak5ActionPerformed(evt);
            }
        });
        getContentPane().add(buttonEsktrak5, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 290, -1, -1));

        jfilterreg.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Filter3x3", "Filter5x5", "Filter7x7" }));
        jfilterreg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jfilterregActionPerformed(evt);
            }
        });
        getContentPane().add(jfilterreg, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 120, -1, -1));

        jcomlabel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1. Bahagia", "2. Sedih", "3. Terkejut", "4. Takut", "5. Marah", "6. Muak" }));
        jcomlabel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcomlabelActionPerformed(evt);
            }
        });
        getContentPane().add(jcomlabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 70, -1, -1));

        labelNama12.setText("Citra Wajah VQ");
        getContentPane().add(labelNama12, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 30, -1, -1));

        labelNama13.setText("Ekspresi :");
        getContentPane().add(labelNama13, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 50, -1, -1));

        labelNama14.setText("Filter Sub Region :");
        getContentPane().add(labelNama14, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 100, -1, -1));

        labelNama1.setText("Gambar Asli");
        getContentPane().add(labelNama1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 260, -1, -1));

        DeteksiObjek.setText("Deteksi Objek");
        DeteksiObjek.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeteksiObjekActionPerformed(evt);
            }
        });
        getContentPane().add(DeteksiObjek, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 260, -1, -1));

        btnTampilCitra.setText("Tampilkan Nilai Citra");
        btnTampilCitra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTampilCitraActionPerformed(evt);
            }
        });
        getContentPane().add(btnTampilCitra, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 290, -1, -1));

        buttonEsktrak6.setText("Tampil Nilai Lowpass Filter");
        buttonEsktrak6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEsktrak6ActionPerformed(evt);
            }
        });
        getContentPane().add(buttonEsktrak6, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 290, -1, -1));

        CitraDeteksi.setText("Tampil Nilai Deteksi Objek");
        CitraDeteksi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CitraDeteksiActionPerformed(evt);
            }
        });
        getContentPane().add(CitraDeteksi, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 290, -1, -1));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnBrowseImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseImageActionPerformed
        // TODO add your handling code here:
        int width;
        int height;
        int newwidth;
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "TITF, JPG, GIF, and PNG Images", "titf", "jpg", "gif", "png");
        chooser.setFileFilter(filter);

        chooser.showOpenDialog(null);
        File file = chooser.getSelectedFile();
        try {
            imageAll = ImageIO.read(file);
            imageWidth = imageAll.getWidth();
            imageHeight = imageAll.getHeight();
            tempImage = file.getAbsolutePath();
        } catch (IOException e) {
            System.out.print(e);
        }

        ImageIcon gambarAsli = gambarAsli(imageAll, 200, 200);
        setGambar.setIcon(gambarAsli);

    }//GEN-LAST:event_btnBrowseImageActionPerformed

    public static String right(String value, int length) {
        // To get right characters from a string, change the begin index.
        return value.substring(value.length() - length);
    }

    public void readImage(String path) throws IOException {
        File file = new File(path);
        imageAll = ImageIO.read(file);
        imageWidth = imageAll.getWidth();
        imageHeight = imageAll.getHeight();
        tempImage = file.getAbsolutePath();
    }

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        // TODO add your handling code here:
        formUtama utama = new formUtama();
        utama.setVisible(true);
        //menghilangkan form_utama
        this.setVisible(false);
    }//GEN-LAST:event_btnCloseActionPerformed

    private void buttonEsktrakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEsktrakActionPerformed
        // TODO add your handling code here:  
        Mat imageLoad = Highgui.imread(tempImage, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
        double[] ekstraksiFitur = null;
        try {
            ekstraksiFitur = ekstrak(imageLoad);
        } catch (IOException ex) {
            Logger.getLogger(forminputLatih.class.getName()).log(Level.SEVERE, null, ex);
        }
        String label = labelLatih;
        String fitur = StringUtils.join(ArrayUtils.toObject(ekstraksiFitur), "\t");
        String fiturMSF = label + ":" + fitur;
        BufferedWriter bw;
        try {
            bw = new BufferedWriter(new FileWriter("fitur/FiturMSFFilter" + filterBlok + ".txt", true));
            bw.write(fiturMSF);
            bw.newLine();
            bw.flush();
        } catch (IOException ex) {
            Logger.getLogger(forminputLatih.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_buttonEsktrakActionPerformed

    private void btnLatihMasalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLatihMasalActionPerformed
        try {
            eksekusiTrain(direktoriBahagia, 1);
            eksekusiTrain(direktoriSedih, 2);
            eksekusiTrain(direktoriTerkejut, 3);
            eksekusiTrain(direktoriTakut, 4);
            eksekusiTrain(direktoriMarah, 5);
            eksekusiTrain(direktoriMuak, 6);
        } catch (IOException ex) {
            Logger.getLogger(forminputLatih.class.getName()).log(Level.SEVERE, null, ex);
        }

        JOptionPane.showMessageDialog(rootPane, "Data Berhasil Dilatih!");

    }//GEN-LAST:event_btnLatihMasalActionPerformed

    public void eksekusiTrain(String direktori, int label) throws IOException {
        int counts = 0;
        for (File data : new File(direktori).listFiles()) {
            counts++;
            readImage(data.getAbsolutePath());
            Mat imageLoad = Highgui.imread(tempImage, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
            double[] ekstraksiFitur = ekstrak(imageLoad);
            String fitur = StringUtils.join(ArrayUtils.toObject(ekstraksiFitur), "\t");
            String fiturMSF = label + ":" + fitur;
            System.out.println(data.getAbsolutePath());

            BufferedWriter bw;
            try {
                bw = new BufferedWriter(new FileWriter("fitur/FiturMSFFilter" + filterBlok + ".txt", true));
                bw.write(fiturMSF);
                bw.newLine();
                bw.flush();
            } catch (IOException ex) {

                Logger.getLogger(forminputLatih.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void lowpassFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lowpassFilterActionPerformed
        // TODO add your handling code here:
        if ("".equals(tempImage)) {
            JOptionPane.showMessageDialog(rootPane, "Masukan Citra Terlebih Dahulu!");
        }
        if (ikonAsli == null) {
            JOptionPane.showMessageDialog(rootPane, "Lakukan Proses Deteksi Terlebih Dahulu");
        }

        Mat matBlur = img2Mat(ikonAsli);
        Imgproc.blur(matBlur, matBlur, new Size(3, 3), new Point(-1, -1));
        Highgui.imwrite("tempBlur.jpg", matBlur);
        ikonBlur = Mat2BufferedImage(matBlur);
        ImageIcon gambarAsli = gambarAsli(ikonBlur, 200, 200);
        setLowpass.setIcon(gambarAsli);
    }//GEN-LAST:event_lowpassFilterActionPerformed

    private void buttonEsktrak4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEsktrak4ActionPerformed
        try {
            // TODO add your handling code here:
            int[][] vqResult = VectorQuantization("tempBlur.jpg");
             Mat imageLoad = Highgui.imread("tempBlur.jpg", Highgui.CV_LOAD_IMAGE_GRAYSCALE);
             Highgui.imwrite("tempVQ.jpg", imageLoad);
            ikonVQ = getBuffered(vqResult);
            ImageIcon gambarAsli = gambarAsli(ikonVQ, 200, 200);
            setVQ.setIcon(gambarAsli);
        } catch (IOException ex) {
            Logger.getLogger(forminputLatih.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_buttonEsktrak4ActionPerformed

    private void buttonEsktrak5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEsktrak5ActionPerformed
        // TODO add your handling code here:
        int[][] citraRGB = img2Array(ikonVQ, ikonVQ.getHeight(), ikonVQ.getWidth());
        new formTampilVQ(citraRGB).setVisible(true);
    }//GEN-LAST:event_buttonEsktrak5ActionPerformed

    private void jfilterregActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jfilterregActionPerformed
        // TODO add your handling code here:
        String itemText = right((String) jfilterreg.getSelectedItem(), 1);
        filterBlok = Integer.parseInt(itemText);
    }//GEN-LAST:event_jfilterregActionPerformed

    private void jcomlabelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcomlabelActionPerformed
        // TODO add your handling code here:
        // TODO add your handling code here:
        String itemText = (String) jcomlabel.getSelectedItem();
        labelLatih = itemText.substring(0, 1);

    }//GEN-LAST:event_jcomlabelActionPerformed

    private void DeteksiObjekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeteksiObjekActionPerformed
        // TODO add your handling code here:
        if ("".equals(tempImage)) {
            JOptionPane.showMessageDialog(rootPane, "Masukan Citra Terlebih Dahulu!");
        }
        Mat imageLoad = Highgui.imread(tempImage, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
        Mat matWajah = deteksiObjek(imageLoad);
        Highgui.imwrite("gambarCrop.jpg", matWajah);
        BufferedImage image = Mat2BufferedImage(imageLoad);
        Rectangle rectWajah = detDebug(imageLoad);
        ikonAsli = cropImage(image, rectWajah);
        ImageIcon gambarAsli = gambarAsli(ikonAsli, 200, 200);
        setlDeteksiObjekWajah.setIcon(gambarAsli);
    }//GEN-LAST:event_DeteksiObjekActionPerformed


    private void btnTampilCitraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTampilCitraActionPerformed
        // TODO add your handling code here:
        String[][] citraRGB = img2ArrayRGB(imageAll, imageAll.getHeight(), imageAll.getWidth());
        new formTampilRGB(citraRGB).setVisible(true);
    }//GEN-LAST:event_btnTampilCitraActionPerformed

    private void buttonEsktrak6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEsktrak6ActionPerformed
        // TODO add your handling code here:
        int[][] citraRGB = img2Array(ikonBlur, ikonBlur.getHeight(), ikonBlur.getWidth());
        new formTampillowpass(citraRGB).setVisible(true);
    }//GEN-LAST:event_buttonEsktrak6ActionPerformed

    private void CitraDeteksiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CitraDeteksiActionPerformed
        // TODO add your handling code here:
        String[][] citraRGB = img2ArrayRGB(ikonAsli, ikonAsli.getHeight(), ikonAsli.getWidth());
        new formTampilDeteksiWajah(citraRGB).setVisible(true);
    }//GEN-LAST:event_CitraDeteksiActionPerformed

    public static String implodeArrayUsingStringUtils(double[] arrayToImplode, String separator) {
        return StringUtils.join(arrayToImplode, separator);
    }

    public double[] ekstrak(Mat imageLoad) throws IOException {
        Mat matWajah = deteksiObjek(imageLoad);
        String tempWajah = "tempWajah.jpg";
        Highgui.imwrite(tempWajah, matWajah);
        Imgproc.blur(matWajah, matWajah, new Size(3, 3), new Point(-1, -1));

        int[][] vqResult = VectorQuantization(tempWajah);
        int x = vqResult.length / filterBlok;
        int y = vqResult[0].length / filterBlok;
        int xreal = vqResult.length;
        int yreal = vqResult[0].length;
        BufferedImage tempImg = getBuffered(vqResult);
        ImageIcon tempImg2 = resizeGambar(tempImg, x * filterBlok, y * filterBlok);
        BufferedImage bi = new BufferedImage(
                tempImg2.getIconWidth(),
                tempImg2.getIconHeight(),
                BufferedImage.TYPE_INT_RGB);
        Graphics g = bi.createGraphics();
        tempImg2.paintIcon(null, g, 0, 0);
        g.dispose();

        int[][] vqrest = img2Array(bi, bi.getWidth(), bi.getHeight());

        List blokMat = matrixToBlocks(vqrest, x, y, xreal, yreal);
        ArrayList<double[]> blokFitur = new ArrayList<>();

        for (int i = 0; i < blokMat.size(); i++) {
            int[][] matBlok = (int[][]) blokMat.get(i);
            int[][] citra32 = createMat32(matBlok, bitMatriks);
            ArrayList InitialDist = initGab(citra32, bitMatriks);
            double[] initialDist0 = (double[]) InitialDist.get(0);
            double[] initialDist45 = (double[]) InitialDist.get(1);
            double[] initialDist90 = (double[]) InitialDist.get(2);
            double[] initialDist135 = (double[]) InitialDist.get(3);
            ArrayList gab = creategabMat(citra32, bitMatriks);
            double[][] transMat0 = (double[][]) gab.get(0);
            double[][] transMat45 = (double[][]) gab.get(1);
            double[][] transMat90 = (double[][]) gab.get(2);
            double[][] transMat135 = (double[][]) gab.get(3);
            int N = bitMatriks + 1;
//
            double[] statDist0 = statDistFunc(transMat0, N);
            double[] statDist45 = statDistFunc(transMat45, N);
            double[] statDist90 = statDistFunc(transMat90, N);
            double[] statDist135 = statDistFunc(transMat135, N);
            double[] fitur0 = ArrayUtils.addAll(initialDist0, statDist0);
            double[] fitur45 = ArrayUtils.addAll(initialDist45, statDist45);
            double[] fitur90 = ArrayUtils.addAll(initialDist90, statDist90);
            double[] fitur135 = ArrayUtils.addAll(initialDist135, statDist135);
            double[] fitur045 = ArrayUtils.addAll(fitur0, fitur45);
            double[] fitur90135 = ArrayUtils.addAll(fitur90, fitur135);
            double[] fitur04590135 = ArrayUtils.addAll(fitur045, fitur90135);
            blokFitur.add(fitur04590135);
        }
        int arrayLenght = blokFitur.get(0).length * blokFitur.size();
        double[] MsfFeature = new double[arrayLenght];
        for (int i = 0; i < blokFitur.size(); i++) {
            double[] arr = blokFitur.get(i);
            int size = arr.length;
            int batasAwal = i * size;
            x = 0;
            for (int j = batasAwal; j < batasAwal + size; j++) {
                MsfFeature[j] = arr[x];
                x++;
            }
        }
        return MsfFeature;
    }

    public static List<int[][]> matrixToBlocks(int[][] yuvMatrix, int x, int y, int xreal, int yreal) {
        List<int[][]> blocks;
        blocks = new ArrayList<>();
////
////        //Iterate over the blocks in row-major order (down first, then right)
        for (int c = 0; c < yuvMatrix.length; c += x) {
            for (int r = 0; r < yuvMatrix[c].length; r += y) {
                int[][] subBlock = new int[x][y];

                for (int bc = 0; bc < x; bc++) {
                    for (int br = 0; br < y; br++) {
                        subBlock[bc][br] = yuvMatrix[c + bc][r + br];
                    }
                }

                blocks.add(subBlock);
            }
        }

        return blocks;
    }

    public double[] statDistFunc(double[][] matRix, int N) {
        Matrix A = new Matrix(matRix);
        A = A.transpose();
        Matrix x = new Matrix(N, 1, 1.0 / N); // initial guess for eigenvector
        for (int i = 0; i < 50; i++) {
            x = A.times(x);
            x = x.times(1.0 / x.norm1());
            // rescale
        }

        double[][] doubMSF = x.getArray();
        double[] newFit = new double[doubMSF.length];
        for (int i = 0; i < doubMSF.length; i++) {
            newFit[i] = doubMSF[i][0];
        }
        double[] newfitStat = Arrays.copyOf(newFit, newFit.length);
        return newfitStat;
    }

    public static double[][] convertFloatsToDoubles(float[][] input) {
        if (input == null) {
            return null; // Or throw an exception - your choice
        }
        double[][] output = new double[input.length][input[0].length];
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[0].length; j++) {
                output[i][j] = input[i][j];
            }
        }
        return output;
    }

    public ArrayList initGab(int[][] citra32, int bitMatriks) {
        int[][] cocu0 = createCoOccuranceMatrix(0, citra32, bitMatriks);
        int[][] cocu45 = createCoOccuranceMatrix(45, citra32, bitMatriks);
        int[][] cocu90 = createCoOccuranceMatrix(90, citra32, bitMatriks);
        int[][] cocu135 = createCoOccuranceMatrix(135, citra32, bitMatriks);
        double[] init0 = initDistFunc(cocu0);
        double[] init45 = initDistFunc(cocu45);
        double[] init90 = initDistFunc(cocu90);
        double[] init135 = initDistFunc(cocu135);
        ArrayList initsgab = new ArrayList();
        initsgab.add(init0);
        initsgab.add(init45);
        initsgab.add(init90);
        initsgab.add(init135);
        return initsgab;
    }

    public double[] initDistFunc(int[][] comat) {
        double[] initdist = new double[comat.length];
        int idx = 0;
        for (int i = 0; i < comat.length; i++) {
            for (int j = 0; j < comat[0].length; j++) {
                initdist[i] = comat[i][i];
            }
        }
        double suminist = (double) Arrays.stream(initdist).sum();
        for (int i = 0; i < initdist.length; i++) {
            initdist[i] = initdist[i] / suminist;
        }
        double[] inits = Arrays.copyOf(initdist, initdist.length);

        return inits;
    }

    public ArrayList creategabMat(int[][] citra32, int bitMatriks) {

        int[][] cocu0 = createCoOccuranceMatrix(0, citra32, bitMatriks);
        int[][] cocuTrans0 = transposeMatrix(cocu0);
        float[][] cocuTransit0 = transitMat(cocuTrans0);
        double[][] cocuTransit0conv = convertFloatsToDoubles(cocuTransit0);

        int[][] cocu45 = createCoOccuranceMatrix(45, citra32, bitMatriks);
        int[][] cocuTrans45 = transposeMatrix(cocu45);
        float[][] cocuTransit45 = transitMat(cocuTrans45);
        double[][] cocuTransit45conv = convertFloatsToDoubles(cocuTransit45);

        int[][] cocu90 = createCoOccuranceMatrix(90, citra32, bitMatriks);
        int[][] cocuTrans90 = transposeMatrix(cocu90);
        float[][] cocuTransit90 = transitMat(cocuTrans90);
        double[][] cocuTransit90conv = convertFloatsToDoubles(cocuTransit90);

        int[][] cocu135 = createCoOccuranceMatrix(135, citra32, bitMatriks);
        int[][] cocuTrans135 = transposeMatrix(cocu135);
        float[][] cocuTransit135 = transitMat(cocuTrans135);
        double[][] cocuTransit135conv = convertFloatsToDoubles(cocuTransit135);

        ArrayList gabco = new ArrayList();
        gabco.add(cocuTransit0conv);
        gabco.add(cocuTransit45conv);
        gabco.add(cocuTransit90conv);
        gabco.add(cocuTransit135conv);

        return gabco;
    }

    public float[][] transitMat(int[][] array) {
        float[][] transMat = new float[array.length][array[0].length];
        for (int i = 0; i < array.length; i++) {
            float sumBaris;
            try {
                sumBaris = Arrays.stream(array[i]).sum();
            } catch (Exception e) {
                sumBaris = 0;
            }
            for (int j = 0; j < array[0].length; j++) {
                if (sumBaris == 0) {
                    transMat[i][j] = 0;
                } else {
                    Float aij = new Float(array[i][j]);
                    transMat[i][j] = aij / sumBaris;

                }
            }
        }
        return transMat;
    }

    public int[][] createCoOccuranceMatrix(int angle, int[][] grayLeveledMatrix, int grayLevel) { //distance = 1
        int[][] temp = new int[grayLevel + 1][grayLevel + 1];
        int startRow = 0;
        int startColumn = 0;
        int endColumn = 0;

        boolean validAngle = true;
        switch (angle) {
            case 0:
                startRow = 0;
                startColumn = 0;
                endColumn = grayLeveledMatrix[0].length - 2;
                break;
            case 45:
                startRow = 1;
                startColumn = 0;
                endColumn = grayLeveledMatrix[0].length - 2;
                break;
            case 90:
                startRow = 1;
                startColumn = 0;
                endColumn = grayLeveledMatrix[0].length - 1;
                break;
            case 135:
                startRow = 1;
                startColumn = 1;
                endColumn = grayLeveledMatrix[0].length - 1;
                break;
            default:
                validAngle = false;
                break;
        }

        if (validAngle) {
            for (int i = startRow; i < grayLeveledMatrix.length; i++) {
                for (int j = startColumn; j <= endColumn; j++) {
                    switch (angle) {
                        case 0:
                            temp[grayLeveledMatrix[i][j]][grayLeveledMatrix[i][j + 1]]++;
                            break;
                        case 45:
                            temp[grayLeveledMatrix[i][j]][grayLeveledMatrix[i - 1][j + 1]]++;
                            break;
                        case 90:
                            temp[grayLeveledMatrix[i][j]][grayLeveledMatrix[i - 1][j]]++;
                            break;
                        case 135:
                            temp[grayLeveledMatrix[i][j]][grayLeveledMatrix[i - 1][j - 1]]++;
                            break;
                    }
                }
            }
        }
        return temp;
    }

    public int[][] transposeMatrix(int[][] m) {
        int[][] temp = new int[m[0].length][m.length];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                temp[j][i] = m[i][j];
            }
        }
        return temp;
    }

    public int[][] add(int[][] m2, int[][] m1) {
        int[][] temp = new int[m1[0].length][m1.length];
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[0].length; j++) {
                temp[j][i] = m1[i][j] + m2[i][j];
            }
        }
        return temp;
    }

    private int getTotal(int[][] m) {
        int temp = 0;
        for (int[] m1 : m) {
            for (int j = 0; j < m[0].length; j++) {
                temp += m1[j];
            }
        }
        return temp;
    }

    private double[][] normalizeMatrix(int[][] m) {
        double[][] temp = new double[m[0].length][m.length];
        int total = getTotal(m);
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                temp[j][i] = (double) m[i][j] / total;
            }
        }
        return temp;
    }

    public int[][] createMat32(int[][] arrMata, int grayLevel) {
        int[][] newMat32 = new int[arrMata.length][arrMata[0].length];
        for (int i = 0; i < arrMata.length; i++) {
            for (int j = 0; j < arrMata[0].length; j++) {
                if (grayLevel > 0 && grayLevel < 255) {
                    newMat32[i][j] = arrMata[i][j] * grayLevel / 255;
                } else {
                    newMat32[i][j] = arrMata[i][j];
                }
            }
        }
        return newMat32;
    }

    public Mat deteksiObjek(Mat imageDet) {
        CascadeClassifier faceDetector = new CascadeClassifier(dirWajah);

        //Deteksi Objek 
        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(imageDet, faceDetections);

        int i = 0;
        Rect faceRect = null;
        for (Rect face : faceDetections.toArray()) {
            faceRect = new Rect(face.x, face.y, face.width, face.height);
        }

        Mat matWajah = new Mat(imageDet, faceRect);
        return matWajah;
    }

    public Rectangle detDebug(Mat imageDet) {
        CascadeClassifier faceDetector = new CascadeClassifier(dirWajah);

        //Deteksi Objek 
        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(imageDet, faceDetections);

        int i = 0;
        Rect faceRect = null;
        int facex = 0;
        int facey = 0;
        int facewidth = 0;
        int faceheight = 0;
        for (Rect face : faceDetections.toArray()) {
            faceRect = new Rect(face.x, face.y, face.width, face.height);
            facex = face.x;
            facey = face.y;
            facewidth = face.width;
            faceheight = face.height;
        }
        Rectangle box = new Rectangle(facex, facey, facewidth, faceheight);

        return box;
    }

    public int[][] VectorQuantization(String tempWajah) throws IOException {
        ArrayList gambarSkalaWajah = ImageScala(vectorHeight, vectorWidth, tempWajah);
        int[][] scaledImageWajah = (int[][]) gambarSkalaWajah.get(2);
        int scaledHeightWajah = (int) gambarSkalaWajah.get(0);
        int scaledWidthWajah = (int) gambarSkalaWajah.get(1);
        Vector<Vector<Integer>> WajahVectors = vektorBlok(scaledHeightWajah, scaledWidthWajah, vectorHeight, vectorWidth, scaledImageWajah);

        Vector<Vector<Integer>> WajahQuantized = new Vector<>();
        Quantize(codeBlockSize, WajahVectors, WajahQuantized);
        Vector<Integer> WajahIndices = Optimize(WajahVectors, WajahQuantized);
        ////////////REWRITE
        int[][] createImageWajah = writeImage(WajahIndices, WajahQuantized, vectorHeight, vectorWidth, scaledWidthWajah, scaledHeightWajah);
        return createImageWajah;
    }

    public int[][] writeImage(Vector<Integer> VectorsToQuantizedIndices, Vector<Vector<Integer>> Quantized, int vectorHeight, int vectorWidth, int scaledWidth, int scaledHeight) {
        int[][] newImg = new int[scaledHeight][scaledWidth];
        for (int i = 0; i < VectorsToQuantizedIndices.size(); i++) {
            int x = i / (scaledWidth / vectorWidth);
            int y = i % (scaledWidth / vectorWidth);
            x *= vectorHeight;
            y *= vectorWidth;
            int v = 0;
            for (int j = x; j < x + vectorHeight; j++) {
                for (int k = y; k < y + vectorWidth; k++) {
                    newImg[j][k] = Quantized.get(VectorsToQuantizedIndices.get(i)).get(v++);
                }
            }
        }
        return newImg;
    }

    public Vector<Vector<Integer>> vektorBlok(int scaledHeight, int scaledWidth, int vectorHeight, int vectorWidth, int[][] scaledImage) {
        Vector<Vector<Integer>> Vectors = new Vector<>();

        //Divide into Vectors and fill The Array Of Vectors
        for (int i = 0; i < scaledHeight; i += vectorHeight) {
            for (int j = 0; j < scaledWidth; j += vectorWidth) {
                Vectors.add(new Vector<>());
                for (int x = i; x < i + vectorHeight; x++) {
                    for (int y = j; y < j + vectorWidth; y++) {

                        Vectors.lastElement().add(scaledImage[x][y]);
                    }
                }
            }
        }

        return Vectors;

    }

    private static Vector<Integer> Optimize(Vector<Vector<Integer>> Vectors, Vector<Vector<Integer>> Quantized) {
        Vector<Integer> VectorsToQuantizedIndices = new Vector<>();

        Vectors.stream().map((vector) -> {
            int smallestDistance = EuclidDistance(vector, Quantized.get(0));
            int smallestIndex = 0;
            //Find the minimum Distance
            for (int i = 1; i < Quantized.size(); i++) {
                int tempDistance = EuclidDistance(vector, Quantized.get(i));
                if (tempDistance < smallestDistance) {
                    smallestDistance = tempDistance;
                    smallestIndex = i;
                }
            }
            return smallestIndex;
        }).forEachOrdered((smallestIndex) -> {
            //Map the i'th Vector to the [i] in Quantized
            VectorsToQuantizedIndices.add(smallestIndex);
        });
        return VectorsToQuantizedIndices;
    }

    private static int EuclidDistance(Vector<Integer> x, Vector<Integer> y) {
        return EuclidDistance(x, y, 0);
    }

    private static void Quantize(int Level, Vector<Vector<Integer>> Vectors, Vector<Vector<Integer>> Quantized) {
        if (Level == 1 || Vectors.isEmpty()) {
            if (Vectors.size() > 0) {
                Quantized.add(vectorAverage(Vectors));
            }
            return;
        }
        //Split
        Vector<Vector<Integer>> leftVectors = new Vector<>();
        Vector<Vector<Integer>> rightVectors = new Vector<>();

        //Calculate Average Vector
        Vector<Integer> mean = vectorAverage(Vectors);

        //Calculate Euclidean Distance
        Vectors.forEach((vec) -> {
            int eDistance1 = EuclidDistance(vec, mean, 1);
            int eDistance2 = EuclidDistance(vec, mean, -1);
            //Add To Right OR Left Vector
            if (eDistance1 >= eDistance2) {
                leftVectors.add(vec);
            } else {
                rightVectors.add(vec);
            }
        });

        //Recurse
        Quantize(Level / 2, leftVectors, Quantized);
        Quantize(Level / 2, rightVectors, Quantized);
    }

    private static Vector<Integer> vectorAverage(Vector<Vector<Integer>> Vectors) {
        int[] summation = new int[Vectors.get(0).size()];

        Vectors.forEach((vector) -> {
            for (int i = 0; i < vector.size(); i++) {
                summation[i] += vector.get(i);
            }
        });

        Vector<Integer> returnVector = new Vector<>();
        for (int i = 0; i < summation.length; i++) {
            returnVector.add(summation[i] / Vectors.size());
        }

        return returnVector;
    }

    public ArrayList ImageScala(int vectorHeight, int vectorWidth, String Path) throws IOException {
        BufferedImage tmpImage = ImageIO.read(new File(Path));
        int originalHeight = tmpImage.getHeight();
        int originalWidth = tmpImage.getWidth();

        int scaledHeight = originalHeight % vectorHeight == 0 ? originalHeight : ((originalHeight / vectorHeight) + 1) * vectorHeight;
        int scaledWidth = originalWidth % vectorWidth == 0 ? originalWidth : ((originalWidth / vectorWidth) + 1) * vectorWidth;

        int[][] image = img2Array(tmpImage, originalHeight, originalWidth);
        int[][] scaledImage = new int[scaledHeight][scaledWidth];
        for (int i = 0; i < scaledHeight; i++) {
            int x = i >= originalHeight ? originalHeight - 1 : i;
            for (int j = 0; j < scaledWidth; j++) {
                int y = j >= originalWidth ? originalWidth - 1 : j;
                scaledImage[i][j] = image[x][y];
            }
        }
        ArrayList listskala = new ArrayList();
        listskala.add(scaledHeight);
        listskala.add(scaledWidth);
        listskala.add(scaledImage);

        return listskala;

    }

    private static int EuclidDistance(Vector<Integer> x, Vector<Integer> y, int incrementFactor) {
        int distance = 0;
        for (int i = 0; i < x.size(); i++) {
            distance += Math.pow(x.get(i) - y.get(i) + incrementFactor, 2);
        }
        return (int) Math.sqrt(distance);
    }

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(forminputLatih.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(() -> {
            new forminputLatih().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CitraDeteksi;
    private javax.swing.JButton DeteksiObjek;
    private javax.swing.JButton btnBrowseImage;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnLatihMasal;
    private javax.swing.JButton btnTampilCitra;
    private javax.swing.JButton buttonEsktrak;
    private javax.swing.JButton buttonEsktrak4;
    private javax.swing.JButton buttonEsktrak5;
    private javax.swing.JButton buttonEsktrak6;
    private javax.swing.JComboBox<String> jcomlabel;
    private javax.swing.JComboBox<String> jfilterreg;
    private javax.swing.JLabel labelNama;
    private javax.swing.JLabel labelNama1;
    private javax.swing.JLabel labelNama11;
    private javax.swing.JLabel labelNama12;
    private javax.swing.JLabel labelNama13;
    private javax.swing.JLabel labelNama14;
    private javax.swing.JLabel labelNama2;
    private javax.swing.JLabel labelNama8;
    private javax.swing.JButton lowpassFilter;
    private javax.swing.JLabel setGambar;
    private javax.swing.JLabel setLowpass;
    private javax.swing.JLabel setVQ;
    private javax.swing.JLabel setlDeteksiObjekWajah;
    // End of variables declaration//GEN-END:variables

}
