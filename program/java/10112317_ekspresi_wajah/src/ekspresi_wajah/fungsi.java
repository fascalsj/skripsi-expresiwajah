/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekspresi_wajah;

import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import javax.swing.ImageIcon;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

/**
 *
 * @author fascalCbn
 */
public class fungsi {

    public static final String dirWajah = "lib/haarcascade_frontalface_alt.xml";
    public static final String dirHidung = "lib/haarcascade_mcs_nose.xml";
    public static final String dirMata = "lib/haarcascade_mcs_lefteye.xml";

    static ImageIcon resizeGambar(BufferedImage image, int width, int height) {
        ImageIcon imageAsli = new ImageIcon(image.getScaledInstance(width, height, Image.SCALE_DEFAULT));
        return imageAsli;
    }

    static Mat img2Mat(BufferedImage in) {
        Mat out = new Mat(in.getHeight(), in.getWidth(), CvType.CV_8UC3);
        byte[] data = new byte[in.getWidth() * in.getHeight() * (int) out.elemSize()];
        int[] dataBuff = in.getRGB(0, 0, in.getWidth(), in.getHeight(), null, 0, in.getWidth());
        for (int i = 0; i < dataBuff.length; i++) {
            data[i * 3] = (byte) ((dataBuff[i]));
            data[i * 3 + 1] = (byte) ((dataBuff[i]));
            data[i * 3 + 2] = (byte) ((dataBuff[i]));
        }
        out.put(0, 0, data);
        return out;
    }

    static Mat img2MatGray(BufferedImage in) {
        Mat out = new Mat(in.getHeight(), in.getWidth(), CvType.CV_8UC3);
        byte[] data = new byte[in.getWidth() * in.getHeight() * (int) out.elemSize()];
        int[] dataBuff = in.getRGB(0, 0, in.getWidth(), in.getHeight(), null, 0, in.getWidth());
        out.put(0, 0, data);
        return out;
    }

    static int[][] img2Array(BufferedImage image, int height, int width) {
        int[][] warnaGrey = new int[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                Color c = new Color(image.getRGB(j, i));
                int R = (int) (c.getRed());
                warnaGrey[i][j] = R;
            }
        }
        return warnaGrey;
    }

    static String[][] img2ArrayRGB(BufferedImage image, int height, int width) {
        String[][] warna = new String[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                Color c = new Color(image.getRGB(j, i));
                int R = (int) (c.getRed());
                int G = (int) (c.getGreen());
                int B = (int) (c.getBlue());
                warna[i][j] = "R : " + R + "  " + "G : " + G + " " + " B :" + B + "\t";
            }
        }
        return warna;
    }

    static BufferedImage MatToBufferedImage(Mat frame) {
        //Mat() to BufferedImage
        int type = 0;
        if (frame.channels() == 1) {
            type = BufferedImage.TYPE_BYTE_GRAY;
        } else if (frame.channels() == 3) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        BufferedImage image = new BufferedImage(132, 132, type);
        WritableRaster raster = image.getRaster();
        DataBufferByte dataBuffer = (DataBufferByte) raster.getDataBuffer();
        byte[] data = dataBuffer.getData();
        frame.get(0, 0, data);

        return image;
    }

    public static BufferedImage Mat2BufferedImage(Mat m) {
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels() * m.cols() * m.rows();
        byte[] b = new byte[bufferSize];
        m.get(0, 0, b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;
    }

    static ImageIcon gambarAsli(BufferedImage image, int width, int height) {
        ImageIcon imageAsli = new ImageIcon(image.getScaledInstance(width, height, Image.SCALE_DEFAULT));

        return imageAsli;
    }

    static void debugGambarArray(int[][] arrImage, String path) throws IOException {
        int arheight = arrImage.length;
        int arwidth = arrImage[0].length;
        BufferedImage newimg = new BufferedImage(arwidth, arheight, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < arheight; i++) {

            for (int j = 0; j < arwidth; j++) {
                Color c = new Color(newimg.getRGB(j, i));
                int R = arrImage[i][j];

                Color newColor = new Color(R, R, R);
                newimg.setRGB(j, i, newColor.getRGB());
            }
        }
        ImageIO.write(newimg, "jpg", new File(path + ".jpg"));

    }

    static BufferedImage cropImage(BufferedImage src, Rectangle rect) {
        BufferedImage dest = src.getSubimage(rect.x, rect.y, rect.width, rect.height);
        return dest;
    }

    static BufferedImage getBuffered(int[][] arrImage) throws IOException {
        int arheight = arrImage.length;
        int arwidth = arrImage[0].length;
        BufferedImage newimg = new BufferedImage(arwidth, arheight, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < arheight; i++) {

            for (int j = 0; j < arwidth; j++) {
                Color c = new Color(newimg.getRGB(j, i));
                int R = arrImage[i][j];

                Color newColor = new Color(R, R, R);
                newimg.setRGB(j, i, newColor.getRGB());
            }
        }
        return newimg;
    }

}
