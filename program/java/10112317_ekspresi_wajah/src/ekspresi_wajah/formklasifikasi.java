/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekspresi_wajah;

import java.io.File;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import static ekspresi_wajah.fungsi.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.objdetect.CascadeClassifier;
import Jama.Matrix;
import java.awt.Graphics;
import java.util.Arrays;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.opencv.core.CvType;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import org.opencv.ml.CvSVM;
import org.opencv.ml.CvSVMParams;

public class formklasifikasi extends javax.swing.JFrame {

    String dirWajah = "lib/haarcascade_frontalface_alt.xml";
    String direktoriBahagia = "testing/Bahagia";
    String direktoriMarah = "testing/Marah";
    String direktoriMuak = "testing/Muak";
    String direktoriTerkejut = "testing/Terkejut";
    String direktoriSedih = "testing/Sedih";
    String direktoriTakut = "testing/Takut";
    int confMatrix[][] = new int[6][6];
    String tempImage = "";
    BufferedImage imageAll;
    BufferedImage faceImage;
    int codeBlockSize = 33;
    int vectorHeight = 4;
    int vectorWidth = 4;
    int filterBlok = 3;
    int indexPrediksi = 0;
    int bitMatriks = 32;
    int imageWidth;
    int imageHeight;
    Mat matWajah;
    public static final String dirXMLSVM = "XMLSVM/SVM";
    public static CvSVMParams SVMParams = new CvSVMParams();
    CvSVM svm = new CvSVM();
    Mat varIdx = new Mat();
    Mat samIdx = new Mat();

    public formklasifikasi() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnBrowseImage = new javax.swing.JButton();
        setGambar = new javax.swing.JLabel();
        labelNama = new javax.swing.JLabel();
        btnClose = new javax.swing.JButton();
        buttonEsktrak = new javax.swing.JButton();
        labelNama1 = new javax.swing.JLabel();
        btnBrowseImage1 = new javax.swing.JButton();
        labelNama3 = new javax.swing.JLabel();
        labelNama2 = new javax.swing.JLabel();
        labelNama4 = new javax.swing.JLabel();
        pilihFilter = new javax.swing.JComboBox<>();
        labelklasifikasi = new javax.swing.JLabel();
        labelNama5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Menu Latih");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnBrowseImage.setText("Browse");
        btnBrowseImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseImageActionPerformed(evt);
            }
        });
        getContentPane().add(btnBrowseImage, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 10, -1, -1));

        setGambar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getContentPane().add(setGambar, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, 200, 200));

        labelNama.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
        labelNama.setText("?");
        getContentPane().add(labelNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 300, 40, 10));

        btnClose.setBackground(new java.awt.Color(255, 0, 0));
        btnClose.setText("Menu Utama");
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        getContentPane().add(btnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 10, -1, -1));

        buttonEsktrak.setText("Klasifikasi Citra");
        buttonEsktrak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEsktrakActionPerformed(evt);
            }
        });
        getContentPane().add(buttonEsktrak, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 130, -1, 40));

        labelNama1.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
        labelNama1.setText(".");
        getContentPane().add(labelNama1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 230, -1, -1));

        btnBrowseImage1.setText("Browse");
        btnBrowseImage1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseImage1ActionPerformed(evt);
            }
        });
        getContentPane().add(btnBrowseImage1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 10, -1, -1));

        labelNama3.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
        labelNama3.setText(".");
        getContentPane().add(labelNama3, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 40, 20, 30));

        labelNama2.setText("Gambar Asli");
        getContentPane().add(labelNama2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 50, -1, -1));

        labelNama4.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
        labelNama4.setText(".");
        getContentPane().add(labelNama4, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 40, 50, 60));

        pilihFilter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Filter 3x3", "Filter 5x5", "Filter 7x7" }));
        pilihFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pilihFilterActionPerformed(evt);
            }
        });
        getContentPane().add(pilihFilter, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, -1, -1));

        labelklasifikasi.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        labelklasifikasi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelklasifikasi.setText("?");
        labelklasifikasi.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getContentPane().add(labelklasifikasi, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 120, 180, 40));

        labelNama5.setText("Hasil Klasifikasi ");
        getContentPane().add(labelNama5, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 60, -1, -1));

        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 300, 20, 20));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnBrowseImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseImageActionPerformed
        // TODO add your handling code here:
        int width;
        int height;
        int newwidth;
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "TITF, JPG, GIF, and PNG Images", "titf", "jpg", "gif", "png");
        chooser.setFileFilter(filter);

        chooser.showOpenDialog(null);
        File file = chooser.getSelectedFile();
        try {
            imageAll = ImageIO.read(file);
            imageWidth = imageAll.getWidth();
            imageHeight = imageAll.getHeight();
            tempImage = file.getAbsolutePath();
        } catch (IOException e) {
            System.out.print(e);
        }
        Mat imageLoad = Highgui.imread(tempImage, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
        ImageIcon gambarAsli = gambarAsli(imageAll, 200, 200);
        setGambar.setIcon(gambarAsli);
    }//GEN-LAST:event_btnBrowseImageActionPerformed

    public static String right(String value, int length) {
        // To get right characters from a string, change the begin index.
        return value.substring(value.length() - length);
    }

    public void readImage(String path) throws IOException {
        File file = new File(path);
        imageAll = ImageIO.read(file);
        imageWidth = imageAll.getWidth();
        imageHeight = imageAll.getHeight();
        tempImage = file.getAbsolutePath();
    }

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        // TODO add your handling code here:
        formUtama utama = new formUtama();
        utama.setVisible(true);
        //menghilangkan form_utama
        this.setVisible(false);
    }//GEN-LAST:event_btnCloseActionPerformed

    private void buttonEsktrakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEsktrakActionPerformed
        // TODO add your handling code here:  
        fungsiKlasifikasi();

    }//GEN-LAST:event_buttonEsktrakActionPerformed

    public void fungsiKlasifikasi() {
        Mat imageLoad = Highgui.imread(tempImage, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
        double[] ekstraksiFitur = null;
        try {
            ekstraksiFitur = ekstrak(imageLoad);
        } catch (IOException ex) {
            Logger.getLogger(formklasifikasi.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(Arrays.toString(ekstraksiFitur));
        Mat matFit = new Mat(1, ekstraksiFitur.length, CvType.CV_32F);
        matFit.put(0, 0, ekstraksiFitur);
        String XML = dirXMLSVM + filterBlok + ".XML";
        svm.load(XML);
        double hasil = svm.predict(matFit);
        int kelas = (int) hasil;
        String label = "";
        switch (kelas) {
            case 1:
                label = "Bahagia";
                break;
            case 2:
                label = "Sedih";
                break;
            case 3:
                label = "Terkejut";
                break;
            case 4:
                label = "Takut";
                break;
            case 5:
                label = "Marah";
                break;
            case 6:
                label = "Muak";
                break;
            default:
                break;
        }
        labelklasifikasi.setText(label);
    }

    public int fungsiKlasifikasi2() {
        Mat imageLoad = Highgui.imread(tempImage, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
        double[] ekstraksiFitur = null;
        try {
            ekstraksiFitur = ekstrak(imageLoad);
        } catch (IOException ex) {
            Logger.getLogger(formklasifikasi.class.getName()).log(Level.SEVERE, null, ex);
        }
        
//        System.out.println();
        Mat matFit = new Mat(1, ekstraksiFitur.length, CvType.CV_32F);
        matFit.put(0, 0, ekstraksiFitur);
        String XML = dirXMLSVM + filterBlok + ".XML";
        svm.load(XML);
        double hasil = svm.predict(matFit);
        int kelas = (int) hasil;
        return kelas;
//        labelklasifikasi.setText(label);
    }

    private void btnBrowseImage1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseImage1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnBrowseImage1ActionPerformed

    private void pilihFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pilihFilterActionPerformed
        // TODO add your handling code here:
        String itemText = right((String) pilihFilter.getSelectedItem(), 1);
        filterBlok = Integer.parseInt(itemText);
        indexPrediksi = 0;
    }//GEN-LAST:event_pilihFilterActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        indexPrediksi = 0;
        for (int i = 0; i < confMatrix.length; i++) {
            for (int j = 0; j < confMatrix[0].length; j++) {
                confMatrix[i][j] = 0;
            }
        }

        System.out.println("Filter Blok : " + filterBlok);
        try {
            // TODO add your handling code here:
            klasifikasiMasal(direktoriBahagia, 1);
            klasifikasiMasal(direktoriSedih, 2);
            klasifikasiMasal(direktoriTerkejut, 3);
            klasifikasiMasal(direktoriTakut, 4);
            klasifikasiMasal(direktoriMarah, 5);
            klasifikasiMasal(direktoriMuak, 6);
            for (int i = 0; i < confMatrix.length; i++) {
                for (int j = 0; j < confMatrix[0].length; j++) {
                    System.out.print(confMatrix[i][j] + "\t");
                }
                System.out.println();
            }
            JOptionPane.showMessageDialog(rootPane, "Proses Klasifikasi Selesai!");
        } catch (IOException ex) {
            Logger.getLogger(formklasifikasi.class.getName()).log(Level.SEVERE, null, ex);
        }

//        System.out.println(Arrays.deepToString(confMatrix));

    }//GEN-LAST:event_jButton1ActionPerformed

    public void klasifikasiMasal(String direktori, int label) throws IOException {
        int counts = 0;
        for (File data : new File(direktori).listFiles()) {
            counts++;
            readImage(data.getAbsolutePath());
            tempImage = data.getAbsolutePath();
            int LabelPrediksi = fungsiKlasifikasi2();
            String LabelTarget = "";
            String LabelPred = "";
            switch (label) {
                case 1:
                    LabelTarget = "Bahagia";
                    break;
                case 2:
                    LabelTarget = "Sedih";
                    break;
                case 3:
                    LabelTarget = "Terkejut";
                    break;
                case 4:
                    LabelTarget = "Takut";
                    break;
                case 5:
                    LabelTarget = "Marah";
                    break;
                case 6:
                    LabelTarget = "Muak";
                    break;
                default:
                    break;
            }

            switch (LabelPrediksi) {
                case 1:
                    LabelPred = "Bahagia";
                    break;
                case 2:
                    LabelPred = "Sedih";
                    break;
                case 3:
                    LabelPred = "Terkejut";
                    break;
                case 4:
                    LabelPred = "Takut";
                    break;
                case 5:
                    LabelPred = "Marah";
                    break;
                case 6:
                    LabelPred = "Muak";
                    break;
                default:
                    break;
            }
            indexPrediksi++;
            String hasilnya;
            if (label == LabelPrediksi) {
                hasilnya = "Benar";
            } else {
                hasilnya = "Salah";
            }

            confMatrix[label - 1][LabelPrediksi - 1] = confMatrix[label - 1][LabelPrediksi - 1] + 1;
            System.out.println(indexPrediksi + "\t" + LabelTarget + "\t" + LabelPred + "\t" + hasilnya);

        }

    }

    public static String implodeArrayUsingStringUtils(double[] arrayToImplode, String separator) {
        return StringUtils.join(arrayToImplode, separator);
    }

    public double[] ekstrak(Mat imageLoad) throws IOException {
        Mat matWajah = deteksiObjek(imageLoad);
        
        String tempWajah = "tempWajah.jpg";
        Highgui.imwrite(tempWajah, matWajah);
        Imgproc.blur(matWajah, matWajah, new Size(3, 3), new Point(-1, -1));

        int[][] vqResult = VectorQuantization(tempWajah);
        int x = vqResult.length / filterBlok;
        int y = vqResult[0].length / filterBlok;
        int xreal = vqResult.length;
        int yreal = vqResult[0].length;
        BufferedImage tempImg = getBuffered(vqResult);
        ImageIcon tempImg2 = resizeGambar(tempImg, x * filterBlok, y * filterBlok);
        BufferedImage bi = new BufferedImage(
                tempImg2.getIconWidth(),
                tempImg2.getIconHeight(),
                BufferedImage.TYPE_INT_RGB);
        Graphics g = bi.createGraphics();
        tempImg2.paintIcon(null, g, 0, 0);
        g.dispose();

        int[][] vqrest = img2Array(bi, bi.getWidth(), bi.getHeight());

        List blokMat = matrixToBlocks(vqrest, x, y, xreal, yreal);
        ArrayList<double[]> blokFitur = new ArrayList<>();

        for (int i = 0; i < blokMat.size(); i++) {
            int[][] matBlok = (int[][]) blokMat.get(i);
            int[][] citra32 = createMat32(matBlok, bitMatriks);
            ArrayList InitialDist = initGab(citra32, bitMatriks);
            double[] initialDist0 = (double[]) InitialDist.get(0);
            double[] initialDist45 = (double[]) InitialDist.get(1);
            double[] initialDist90 = (double[]) InitialDist.get(2);
            double[] initialDist135 = (double[]) InitialDist.get(3);
            ArrayList gab = creategabMat(citra32, bitMatriks);
            double[][] transMat0 = (double[][]) gab.get(0);
            double[][] transMat45 = (double[][]) gab.get(1);
            double[][] transMat90 = (double[][]) gab.get(2);
            double[][] transMat135 = (double[][]) gab.get(3);
            int N = bitMatriks + 1;
//
            double[] statDist0 = statDistFunc(transMat0, N);
            double[] statDist45 = statDistFunc(transMat45, N);
            double[] statDist90 = statDistFunc(transMat90, N);
            double[] statDist135 = statDistFunc(transMat135, N);
            double[] fitur0 = ArrayUtils.addAll(initialDist0, statDist0);
            double[] fitur45 = ArrayUtils.addAll(initialDist45, statDist45);
            double[] fitur90 = ArrayUtils.addAll(initialDist90, statDist90);
            double[] fitur135 = ArrayUtils.addAll(initialDist135, statDist135);
            double[] fitur045 = ArrayUtils.addAll(fitur0, fitur45);
            double[] fitur90135 = ArrayUtils.addAll(fitur90, fitur135);
            double[] fitur04590135 = ArrayUtils.addAll(fitur045, fitur90135);
            blokFitur.add(fitur04590135);
        }
        int arrayLenght = blokFitur.get(0).length * blokFitur.size();
        double[] MsfFeature = new double[arrayLenght];
        for (int i = 0; i < blokFitur.size(); i++) {
            double[] arr = blokFitur.get(i);
            int size = arr.length;
            int batasAwal = i * size;
            x = 0;
            for (int j = batasAwal; j < batasAwal + size; j++) {
                MsfFeature[j] = arr[x];
                x++;
            }
        }

        return MsfFeature;
    }

    public static List<int[][]> matrixToBlocks(int[][] yuvMatrix, int x, int y, int xreal, int yreal) {
        List<int[][]> blocks = new ArrayList<>();
////
////        //Iterate over the blocks in row-major order (down first, then right)
        for (int c = 0; c < yuvMatrix.length; c += x) {
            for (int r = 0; r < yuvMatrix[c].length; r += y) {
                int[][] subBlock = new int[x][y];

////
////                //Iterate over the block in row-major order
                for (int bc = 0; bc < x; bc++) {
                    for (int br = 0; br < y; br++) {

                        subBlock[bc][br] = yuvMatrix[c + bc][r + br];
                    }
                }
////
                blocks.add(subBlock);
            }
        }

        return blocks;
    }

    public double[] statDistFunc(double[][] matRix, int N) {
        Matrix A = new Matrix(matRix);
        A = A.transpose();
        Matrix x = new Matrix(N, 1, 1.0 / N); // initial guess for eigenvector
        for (int i = 0; i < 50; i++) {
            x = A.times(x);
            x = x.times(1.0 / x.norm1());       // rescale
        }

        double[][] doubMSF = x.getArray();
        double[] newFit = new double[doubMSF.length];
        for (int i = 0; i < doubMSF.length; i++) {
            newFit[i] = doubMSF[i][0];
        }
        double[] newfitStat = Arrays.copyOf(newFit, newFit.length);
        return newfitStat;
    }

    public static double[][] convertFloatsToDoubles(float[][] input) {
        if (input == null) {
            return null; // Or throw an exception - your choice
        }
        double[][] output = new double[input.length][input[0].length];
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[0].length; j++) {
                output[i][j] = input[i][j];
            }
        }
        return output;
    }

    public ArrayList initGab(int[][] citra32, int bitMatriks) {
        int[][] cocu0 = createCoOccuranceMatrix(0, citra32, bitMatriks);
        int[][] cocu45 = createCoOccuranceMatrix(45, citra32, bitMatriks);
        int[][] cocu90 = createCoOccuranceMatrix(90, citra32, bitMatriks);
        int[][] cocu135 = createCoOccuranceMatrix(135, citra32, bitMatriks);
        double[] init0 = initDistFunc(cocu0);
        double[] init45 = initDistFunc(cocu45);
        double[] init90 = initDistFunc(cocu90);
        double[] init135 = initDistFunc(cocu135);
        ArrayList initsgab = new ArrayList();
        initsgab.add(init0);
        initsgab.add(init45);
        initsgab.add(init90);
        initsgab.add(init135);
        return initsgab;
    }

    public double[] initDistFunc(int[][] comat) {
        double[] initdist = new double[comat.length];
        int idx = 0;
        for (int i = 0; i < comat.length; i++) {
            for (int j = 0; j < comat[0].length; j++) {
                initdist[i] = comat[i][i];
            }
        }
        double suminist = (double) Arrays.stream(initdist).sum();
        for (int i = 0; i < initdist.length; i++) {
            initdist[i] = initdist[i] / suminist;
        }
        double[] inits = Arrays.copyOf(initdist, initdist.length);

        return inits;
    }

    public ArrayList creategabMat(int[][] citra32, int bitMatriks) {

        int[][] cocu0 = createCoOccuranceMatrix(0, citra32, bitMatriks);
        int[][] cocuTrans0 = transposeMatrix(cocu0);
        float[][] cocuTransit0 = transitMat(cocuTrans0);
        double[][] cocuTransit0conv = convertFloatsToDoubles(cocuTransit0);

        int[][] cocu45 = createCoOccuranceMatrix(45, citra32, bitMatriks);
        int[][] cocuTrans45 = transposeMatrix(cocu45);
        float[][] cocuTransit45 = transitMat(cocuTrans45);
        double[][] cocuTransit45conv = convertFloatsToDoubles(cocuTransit45);

        int[][] cocu90 = createCoOccuranceMatrix(90, citra32, bitMatriks);
        int[][] cocuTrans90 = transposeMatrix(cocu90);
        float[][] cocuTransit90 = transitMat(cocuTrans90);
        double[][] cocuTransit90conv = convertFloatsToDoubles(cocuTransit90);

        int[][] cocu135 = createCoOccuranceMatrix(135, citra32, bitMatriks);
        int[][] cocuTrans135 = transposeMatrix(cocu135);
        float[][] cocuTransit135 = transitMat(cocuTrans135);
        double[][] cocuTransit135conv = convertFloatsToDoubles(cocuTransit135);

        ArrayList gabco = new ArrayList();
        gabco.add(cocuTransit0conv);
        gabco.add(cocuTransit45conv);
        gabco.add(cocuTransit90conv);
        gabco.add(cocuTransit135conv);

        return gabco;
    }

    public float[][] transitMat(int[][] array) {
        float[][] transMat = new float[array.length][array[0].length];
        for (int i = 0; i < array.length; i++) {
            float sumBaris = 0;
            try {
                sumBaris = Arrays.stream(array[i]).sum();
            } catch (Exception e) {
                sumBaris = 0;
            }
            for (int j = 0; j < array[0].length; j++) {
                if (sumBaris == 0) {
                    transMat[i][j] = 0;
                } else {
                    Float aij = new Float(array[i][j]);
                    transMat[i][j] = aij / sumBaris;

                }
            }
        }
        return transMat;
    }

    public int[][] createCoOccuranceMatrix(int angle, int[][] grayLeveledMatrix, int grayLevel) { //distance = 1
        int[][] temp = new int[grayLevel + 1][grayLevel + 1];
        int startRow = 0;
        int startColumn = 0;
        int endColumn = 0;

        boolean validAngle = true;
        switch (angle) {
            case 0:
                startRow = 0;
                startColumn = 0;
                endColumn = grayLeveledMatrix[0].length - 2;
                break;
            case 45:
                startRow = 1;
                startColumn = 0;
                endColumn = grayLeveledMatrix[0].length - 2;
                break;
            case 90:
                startRow = 1;
                startColumn = 0;
                endColumn = grayLeveledMatrix[0].length - 1;
                break;
            case 135:
                startRow = 1;
                startColumn = 1;
                endColumn = grayLeveledMatrix[0].length - 1;
                break;
            default:
                validAngle = false;
                break;
        }

        if (validAngle) {
            for (int i = startRow; i < grayLeveledMatrix.length; i++) {
                for (int j = startColumn; j <= endColumn; j++) {
                    switch (angle) {
                        case 0:
                            temp[grayLeveledMatrix[i][j]][grayLeveledMatrix[i][j + 1]]++;
                            break;
                        case 45:
                            temp[grayLeveledMatrix[i][j]][grayLeveledMatrix[i - 1][j + 1]]++;
                            break;
                        case 90:
                            temp[grayLeveledMatrix[i][j]][grayLeveledMatrix[i - 1][j]]++;
                            break;
                        case 135:
                            temp[grayLeveledMatrix[i][j]][grayLeveledMatrix[i - 1][j - 1]]++;
                            break;
                    }
                }
            }
        }
        return temp;
    }

    public int[][] transposeMatrix(int[][] m) {
        int[][] temp = new int[m[0].length][m.length];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                temp[j][i] = m[i][j];
            }
        }
        return temp;
    }

    public int[][] add(int[][] m2, int[][] m1) {
        int[][] temp = new int[m1[0].length][m1.length];
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[0].length; j++) {
                temp[j][i] = m1[i][j] + m2[i][j];
            }
        }
        return temp;
    }

    private int getTotal(int[][] m) {
        int temp = 0;
        for (int[] m1 : m) {
            for (int j = 0; j < m[0].length; j++) {
                temp += m1[j];
            }
        }
        return temp;
    }

    private double[][] normalizeMatrix(int[][] m) {
        double[][] temp = new double[m[0].length][m.length];
        int total = getTotal(m);
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                temp[j][i] = (double) m[i][j] / total;
            }
        }
        return temp;
    }

    public int[][] createMat32(int[][] arrMata, int grayLevel) {
        int[][] newMat32 = new int[arrMata.length][arrMata[0].length];
        for (int i = 0; i < arrMata.length; i++) {
            for (int j = 0; j < arrMata[0].length; j++) {
                if (grayLevel > 0 && grayLevel < 255) {
                    newMat32[i][j] = arrMata[i][j] * grayLevel / 255;
                } else {
                    newMat32[i][j] = arrMata[i][j];
                }
            }
        }
        return newMat32;
    }

    public Mat deteksiObjek(Mat imageDet) {
        CascadeClassifier faceDetector = new CascadeClassifier(dirWajah);

        //Deteksi Objek 
        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(imageDet, faceDetections);

        int i = 0;
        Rect faceRect = null;
        for (Rect face : faceDetections.toArray()) {
            faceRect = new Rect(face.x, face.y, face.width, face.height);
        }

        Mat matWajah = new Mat(imageDet, faceRect);
        return matWajah;
    }

//    public void grayscale() {
//        Imgproc.cvtColor(matMata, matMata, Imgproc.COLOR_RGB2GRAY);
//        Imgproc.cvtColor(matMulut, matMulut, Imgproc.COLOR_RGB2GRAY);
//
//    }
//    public void blur() {
//        Imgproc.blur(matMata, matMata, new Size(3, 3), new Point(-1, -1));
//        Imgproc.blur(matMulut, matMulut, new Size(3, 3), new Point(-1, -1));
//    }
    public int[][] VectorQuantization(String tempWajah) throws IOException {
        ArrayList gambarSkalaWajah = ImageScala(vectorHeight, vectorWidth, tempWajah);
        int[][] scaledImageWajah = (int[][]) gambarSkalaWajah.get(2);
        int scaledHeightWajah = (int) gambarSkalaWajah.get(0);
        int scaledWidthWajah = (int) gambarSkalaWajah.get(1);
        Vector<Vector<Integer>> WajahVectors = vektorBlok(scaledHeightWajah, scaledWidthWajah, vectorHeight, vectorWidth, scaledImageWajah);

        Vector<Vector<Integer>> WajahQuantized = new Vector<>();
        Quantize(codeBlockSize, WajahVectors, WajahQuantized);
        Vector<Integer> WajahIndices = Optimize(WajahVectors, WajahQuantized);
        ////////////REWRITE
        int[][] createImageWajah = writeImage(WajahIndices, WajahQuantized, vectorHeight, vectorWidth, scaledWidthWajah, scaledHeightWajah);
        return createImageWajah;
    }

    public int[][] writeImage(Vector<Integer> VectorsToQuantizedIndices, Vector<Vector<Integer>> Quantized, int vectorHeight, int vectorWidth, int scaledWidth, int scaledHeight) {
        int[][] newImg = new int[scaledHeight][scaledWidth];
        for (int i = 0; i < VectorsToQuantizedIndices.size(); i++) {
            int x = i / (scaledWidth / vectorWidth);
            int y = i % (scaledWidth / vectorWidth);
            x *= vectorHeight;
            y *= vectorWidth;
            int v = 0;
            for (int j = x; j < x + vectorHeight; j++) {
                for (int k = y; k < y + vectorWidth; k++) {
                    newImg[j][k] = Quantized.get(VectorsToQuantizedIndices.get(i)).get(v++);
                }
            }
        }
        return newImg;
    }

    public Vector<Vector<Integer>> vektorBlok(int scaledHeight, int scaledWidth, int vectorHeight, int vectorWidth, int[][] scaledImage) {
        Vector<Vector<Integer>> Vectors = new Vector<>();

        //Divide into Vectors and fill The Array Of Vectors
        for (int i = 0; i < scaledHeight; i += vectorHeight) {
            for (int j = 0; j < scaledWidth; j += vectorWidth) {
                Vectors.add(new Vector<>());
                for (int x = i; x < i + vectorHeight; x++) {
                    for (int y = j; y < j + vectorWidth; y++) {

                        Vectors.lastElement().add(scaledImage[x][y]);
                    }
                }
            }
        }

        return Vectors;

    }

    private static Vector<Integer> Optimize(Vector<Vector<Integer>> Vectors, Vector<Vector<Integer>> Quantized) {
        Vector<Integer> VectorsToQuantizedIndices = new Vector<>();

        Vectors.stream().map((vector) -> {
            int smallestDistance = EuclidDistance(vector, Quantized.get(0));
            int smallestIndex = 0;
            //Find the minimum Distance
            for (int i = 1; i < Quantized.size(); i++) {
                int tempDistance = EuclidDistance(vector, Quantized.get(i));
                if (tempDistance < smallestDistance) {
                    smallestDistance = tempDistance;
                    smallestIndex = i;
                }
            }
            return smallestIndex;
        }).forEachOrdered((smallestIndex) -> {
            //Map the i'th Vector to the [i] in Quantized
            VectorsToQuantizedIndices.add(smallestIndex);
        });
        return VectorsToQuantizedIndices;
    }

    private static int EuclidDistance(Vector<Integer> x, Vector<Integer> y) {
        return EuclidDistance(x, y, 0);
    }

    private static void Quantize(int Level, Vector<Vector<Integer>> Vectors, Vector<Vector<Integer>> Quantized) {
        if (Level == 1 || Vectors.isEmpty()) {
            if (Vectors.size() > 0) {
                Quantized.add(vectorAverage(Vectors));
            }
            return;
        }
        //Split
        Vector<Vector<Integer>> leftVectors = new Vector<>();
        Vector<Vector<Integer>> rightVectors = new Vector<>();

        //Calculate Average Vector
        Vector<Integer> mean = vectorAverage(Vectors);

        //Calculate Euclidean Distance
        Vectors.forEach((vec) -> {
            int eDistance1 = EuclidDistance(vec, mean, 1);
            int eDistance2 = EuclidDistance(vec, mean, -1);
            //Add To Right OR Left Vector
            if (eDistance1 >= eDistance2) {
                leftVectors.add(vec);
            } else {
                rightVectors.add(vec);
            }
        });

        //Recurse
        Quantize(Level / 2, leftVectors, Quantized);
        Quantize(Level / 2, rightVectors, Quantized);
    }

    private static Vector<Integer> vectorAverage(Vector<Vector<Integer>> Vectors) {
        int[] summation = new int[Vectors.get(0).size()];

        Vectors.forEach((vector) -> {
            for (int i = 0; i < vector.size(); i++) {
                summation[i] += vector.get(i);
            }
        });

        Vector<Integer> returnVector = new Vector<>();
        for (int i = 0; i < summation.length; i++) {
            returnVector.add(summation[i] / Vectors.size());
        }

        return returnVector;
    }

    public ArrayList ImageScala(int vectorHeight, int vectorWidth, String Path) throws IOException {
        BufferedImage tempImage = ImageIO.read(new File(Path));
        int originalHeight = tempImage.getHeight();
        int originalWidth = tempImage.getWidth();

        int scaledHeight = originalHeight % vectorHeight == 0 ? originalHeight : ((originalHeight / vectorHeight) + 1) * vectorHeight;
        int scaledWidth = originalWidth % vectorWidth == 0 ? originalWidth : ((originalWidth / vectorWidth) + 1) * vectorWidth;

        int[][] image = img2Array(tempImage, originalHeight, originalWidth);
        int[][] scaledImage = new int[scaledHeight][scaledWidth];
        for (int i = 0; i < scaledHeight; i++) {
            int x = i >= originalHeight ? originalHeight - 1 : i;
            for (int j = 0; j < scaledWidth; j++) {
                int y = j >= originalWidth ? originalWidth - 1 : j;
                scaledImage[i][j] = image[x][y];
            }
        }
        ArrayList listskala = new ArrayList();
        listskala.add(scaledHeight);
        listskala.add(scaledWidth);
        listskala.add(scaledImage);

        return listskala;

    }

    private static int EuclidDistance(Vector<Integer> x, Vector<Integer> y, int incrementFactor) {
        int distance = 0;
        for (int i = 0; i < x.size(); i++) {
            distance += Math.pow(x.get(i) - y.get(i) + incrementFactor, 2);
        }
        return (int) Math.sqrt(distance);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(formklasifikasi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new formklasifikasi().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBrowseImage;
    private javax.swing.JButton btnBrowseImage1;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton buttonEsktrak;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel labelNama;
    private javax.swing.JLabel labelNama1;
    private javax.swing.JLabel labelNama2;
    private javax.swing.JLabel labelNama3;
    private javax.swing.JLabel labelNama4;
    private javax.swing.JLabel labelNama5;
    private javax.swing.JLabel labelklasifikasi;
    private javax.swing.JComboBox<String> pilihFilter;
    private javax.swing.JLabel setGambar;
    // End of variables declaration//GEN-END:variables

}
