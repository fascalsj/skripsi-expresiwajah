<canvas></canvas>
<script type="text/javascript">
  function loadImage(url) {
  return new Promise(function(resolve, reject) {
    var image = new Image()
    image.crossOrigin = 'anonymous'
    image.onload = resolve.bind(null, image)
    image.onerror = reject
    image.src = url
  })
}

// http://stackoverflow.com/questions/13917139/fastest-way-to-iterate-pixels-in-a-canvas-and-copy-some-of-them-in-another-one
// http://adrianboeing.blogspot.de/2012/01/kinect-viewer-with-html5-canvas.html
// http://cacodaemon.de/index.php?id=33
function getImageData(image) {
  var canvas = document.createElement('canvas')
  var context = canvas.getContext('2d')
  canvas.width = image.width
  canvas.height = image.height
  context.drawImage(image, 0, 0)
  return context.getImageData(0, 0, image.width, image.height)
}

// http://www.html5canvastutorials.com/advanced/html5-canvas-grayscale-image-colors-tutorial/
function grayscaleImageData(imageData) {
  var data = imageData.data
  for (var i = 0; i < data.length; i += 4) {
    var brightness = 0.34 * data[i] + 0.5 * data[i + 1] + 0.16 * data[i + 2]
    data[i] = brightness
    data[i + 1] = brightness
    data[i + 2] = brightness
  }
  return imageData
}

function renderImageData(context, imageData) {
  context.canvas.width = imageData.width
  context.canvas.height = imageData.height
  context.putImageData(imageData, 0, 0)
  return imageData
}

function getPixelAt(imageData, x, y) {
  return (x + imageData.width * y) * 4
}

var canvas = document.querySelector('canvas')
var context = canvas.getContext('2d')

loadImage('https://unsplash.it/640/480/?image=808')
.then(function(image) {
  return getImageData(image)
})
.then(function (imageData) {
  return grayscaleImageData(imageData)
})
.then(function (imageData) {
  return renderImageData(context, imageData)
})
</script>