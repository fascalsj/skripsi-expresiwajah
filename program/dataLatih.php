<script type="text/javascript">
    function readURL(input)
    {

        if (input.files && input.files[0])
        {
            var reader = new FileReader();

            reader.onload = function (e)
            {
                $('#gambarOlah').attr('src', e.target.result);
                $('#gambarOlah2').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function ()
    {
        readURL(this);
    });
</script>
<div class="card"></div>
<div class="col-md-2 grid-margin">
  <a href="" class="btn btn-success"><i class="mdi mdi-home menu-icon"></i> Menu Utama </a>
</div>
<div class="col-md-2 grid-margin">
    
</div>

</div>
<div class="col-md-12 grid-margin">
    <div class="row">
        <div class="col-md-4 grid-margin stretch-card">
              <div class="card">
                    <div class="card-body">
                        <div class="clearfix">
                            <h4 class="card-title">Citra Wajah</h4>                                 
                        </div>
                        <img style="display: none" id="gambarOlah" src="#" >
                        <img id="gambarOlah2" width="250px" src="#" >
                        <br>
                        <br>
                        <input type='file' id="imgInp" />
                </div>
            </div>
        </div>
        <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="clearfix">
                        <h4 class="card-title float-left">Gambar Crop</h4>                                 
                    </div>
                    <img id="gambarCrop" src="#" >
                    <br>
                    <br>
                    <div class="d-flex mt-12 align-items-top">
                        <a class="btn btn-danger" id="klikDeteksiWajah" href="#">
                            Deteksi Wajah
                        </a> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="clearfix">
                        <h4 class="card-title float-left">Gambar Grayscale</h4>                                 
                    </div>
                    <img id="gambarGrayscale" src="#" >
                    <br>
                    <br>
                    <div class="d-flex mt-12 align-items-top">
                        <a class="btn btn-danger" id="klikGrayscaling" href="#">
                            Grayscaling
                        </a> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="clearfix">
                        <h4 class="card-title float-left">Gambar Lowpass Filter</h4>                                 
                    </div>
                    <img id="gambarLowpass" src="#" >
                    <br>
                    <br>
                    <div class="d-flex mt-12 align-items-top">
                        <a class="btn btn-danger" id="klikLowpass" href="#">
                            Lowpass Filter
                        </a> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="clearfix">
                        <h4 class="card-title float-left">Gambar VQ</h4>                                 
                    </div>
                    <img id="gambarVQ" src="#" >
                    <br>
                    <br>
                    <div class="d-flex mt-12 align-items-top">
                        <a class="btn btn-danger" id="klikVQ" href="#">
                            Generate VQ
                        </a> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="clearfix">
                        <h4 class="card-title float-left">Table RGB</h4>                                 
                    </div>
                    <div class="table-responsive">
                        <div id="rgb"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="clearfix">
                        <h4 class="card-title float-left">Citra Grayscale</h4>                                 
                    </div>
                    <div class="table-responsive">
                        <div id="grayscaled"></div>
                    </div>
                </div>
            </div>
        </div><div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="clearfix">
                        <h4 class="card-title float-left">Citra Lowpass Filter</h4>                                 
                    </div>
                    <div class="table-responsive">
                        <div id="lowpassd"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="clearfix">
                        <h4 class="card-title float-left">Vector Quantization</h4>                                 
                    </div>
                    <div class="table-responsive">
                        <div id="vqdeb"></div>
                    </div>
                </div>
            </div>
        </div><div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="clearfix">
                        <h4 class="card-title float-left">Markov Stationary Feature</h4>                                 
                    </div>
                    <div class="table-responsive">
                        <div id="msfdeb"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
   
    $(function ()
    {   
        $('#klikDeteksiWajah').click(function (e)
        {
            e.preventDefault();
            // if( $('#jenis_latih').val() != '' && $('#imgInp').val() != ''){
                $('.face').remove();
                $('#gambarOlah').faceDetection(
                        {
                            complete: function (faces)
                            {
                                cropWajah = crop(faces)[0];
                                cropCanvas = crop(faces)[1];
                                document.getElementById('gambarCrop').src = cropWajah;
                                RGBdebug(cropCanvas, "rgb");

                                // Canvasgrayscale = grayscale(cropCanvas);
                                // var grayscaleImage = Canvasgrayscale.toDataURL('image/jpeg');
                                // document.getElementById('gambarGrayscale').src = grayscaleImage;
                                // canvasBlur = lowpassFilter(Canvasgrayscale);
                                // var canvasBlur2 = canvasBlur.toDataURL('image/jpeg');
                                // document.getElementById('gambarLowpass').src = canvasBlur2;
                                // canvasVQ = vq_method(canvasBlur);
                                // var canvasVQ2 = canvasVQ[1].toDataURL('image/jpeg');
                                // document.getElementById('gambarVQ').src = canvasVQ2;
                                // grayscaledebug(Canvasgrayscale, "grayscaled");
                                // Lowpassdebug(cropCanvas, "lowpassd");
                                // debug(canvasVQ[0][1], "histogram");
                                
                            },
                            error: function (code, message)
                            {
                                alert('Error: ' + message);
                            }
                        }
                    );
            // }
            // else
            // {
            //     if($('#jenis_latih').val() == '')
            //     {
            //         alert("Pilih Dulu Jenis Data Latih");
            //     } 
            //     if( $('#imgInp').val() == '')
            //     {
            //         alert("Upload dahulu gambar yang akan diproses");
            //     }
                
            // }


        });
         $('#klikGrayscaling').click(function (e)
        {
            image = document.getElementById('gambarCrop');
            conv = convertImageToCanvas(image);
            Canvasgrayscale = grayscale(conv);
            var grayscaleImage = Canvasgrayscale.toDataURL('image/jpeg');
            document.getElementById('gambarGrayscale').src = grayscaleImage;
            grayscaledebug(Canvasgrayscale, "grayscaled");
            e.preventDefault();
        });

        $('#klikLowpass').click(function (e)
        {
            image = document.getElementById('gambarGrayscale');
            conv = convertImageToCanvas(image);
            canvasBlur = lowpassFilter(conv);
            var canvasBlur2 = canvasBlur.toDataURL('image/jpeg');
            document.getElementById('gambarLowpass').src = canvasBlur2;
            Lowpassdebug(canvasBlur, "lowpassd");
            e.preventDefault();
        });

        $('#klikVQ').click(function (e)
        {
            image = document.getElementById('gambarLowpass');
            conv = convertImageToCanvas(image);
            canvasVQ = vq_method(canvasBlur);
            var canvasVQ2 = canvasVQ.toDataURL('image/jpeg');
            document.getElementById('gambarVQ').src = canvasVQ2;
            vqdebuger(canvasVQ, "vqdeb");
            fitur = msf(canvasVQ);
            newarr = [];
            for (var i = 0; i < fitur.length; i++) {
               newarr[i] = [(i+1),fitur[i]];
            }
            msfDebug(newarr, "msfdeb");
            e.preventDefault();
        });
    });

function convertImageToCanvas(image) {
    var canvas = document.createElement("canvas");
    canvas.width = image.width;
    canvas.height = image.height;
    canvas.getContext("2d").drawImage(image, 0, 0);

    return canvas;
}
function convertCanvasToImage(canvas) {
    var image = new Image();
    image.src = canvas.toDataURL("image/png");
    return image;
}

function crop(faces) {
    var top = faces[0].y * faces[0].scaleY;
    var side = faces[0].x * faces[0].scaleX;
    var width = faces[0].width * faces[0].scaleX;
    var height = faces[0].height * faces[0].scaleY;
    image = document.getElementById('gambarOlah');
    var canvasOlah = convertImageToCanvas(image);
    var canvas = canvasOlah,
        context = canvas.getContext('2d'),
        image = new Image();
    canvas.width = canvas.height = 132;

    image.src = document.getElementById('gambarOlah').src;

    context.drawImage(image, side, top, width, height, 0, 0, 132, 132);
    dataUrl = canvas.toDataURL('image/jpeg');
    return [dataUrl, canvas];
}


function grayscale(canvas) {
    var ctx = canvas.getContext("2d");
    imageData = ctx.getImageData(0, 0, canvas.width, canvas.height)
    data = imageData.data;

    for (var i = 0; i < data.length; i += 4) {
        var avg = ((data[i]* 0.29) + (data[i + 1] *0.59) + (data[i + 2]*0.11));
        data[i] = avg; // red
        data[i + 1] = avg; // green
        data[i + 2] = avg; // blue
    }
    return ctx.putImageData(imageData, 0, 0), canvas;

}

function lowpassFilter(canvas) {
    var c = canvas.getContext('2d');
    var height = canvas.height;
    var width = canvas.width;
    var i, j;
    var gambarBaru = [];
    var isigambar;
    for (i = 0; i < (height); i++) {
        gambarBaru[i] = [];
        for (j = 0; j < (width); j++) {

            isigambar = c.getImageData(i, j, 1, 1).data;
            gambarBaru[i][j] = Math.ceil(isigambar[0] * (1 / 9));

        }
    }
    var new_height = gambarBaru.length;
    var new_width = gambarBaru.length;
    var gambarBaruNew = [];

    var ctx = canvas.getContext("2d");
    for (i = 0; i < new_height - 2; i++) {
        gambarBaruNew[i] = [];
        for (j = 0; j < new_width - 2; j++) {
            gambarBaruNew[i][j] = gambarBaru[i][j] + gambarBaru[i][j + 1] + gambarBaru[i][j + 2] + gambarBaru[i + 1][j] + gambarBaru[i + 1][j + 1] + gambarBaru[i + 1][j + 2] + gambarBaru[i + 2][j] + gambarBaru[i + 2][j + 1] + gambarBaru[i + 2][j + 2];
            ctx.fillStyle = 'rgb(' + gambarBaruNew[i][j] + ', ' + gambarBaruNew[i][j] + ', ' + gambarBaruNew[i][j] + ')';
            ctx.fillRect(i, j, 1, 1);
        }
    }
    imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    data = imageData.data;
    return ctx.putImageData(imageData, 0, 0), canvas;
}

function vq_method(canvas) {

    var c = canvas.getContext('2d');
    var col = canvas.height;
    var row = canvas.width;
    var size = col * row;
    var i, j, x, y;

    var vector = [];
    var inn = 0;
    for (i = 0; i < (col); i++) {
        vector[i] = [];
        for (j = 0; j < (row); j++) {
            vector[i][j] = c.getImageData(i, j, 1, 1).data[0];
            inn + 3;
        }
    }


    //deklarasi vector
    var A = vector;

    N = row * col;
    p = 4;
    n = p * p;
    Nc = 33;
    Nb = N / n;
    tSet = [];

    for (i = 0; i < Nb; i++) {
        tSet[i] = [];
        for (j = 0; j < n; j++) {
            tSet[i][j] = 0;
        }
    }
    
    indexCM = 0;
    for (i = 0; i < row; i += p) {
        for (j = 0; j < col; j += p) {
            temp = getTV(i, j, A, n, p);
            for (l = 0; l < n; l++) {

                tSet[indexCM][l] = temp[0][l];
            }

            indexCM = indexCM + 1;
        }
    }


    temp = [];
    for (i = 0; i < Nc; i++) {
        temp.push(Math.floor(Math.random() * Nb));
    }

    // Membuat Codebook
    indexCM = 0;
    var codebook = [];
    for (i = 0; i < Nc; i++) {
        codebook[i] = [];
        for (j = 0; j < n; j++) {
            codebook[i][j] = tSet[temp[i]][j];
        }

    }
    var codebook = [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,128,255,0,0,128,255,0,0,128,255,0,0,128,255],
[255,128,0,0,255,128,0,0,255,128,0,0,255,128,0,0],
[255,255,255,255,128,128,128,128,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,255,255,255,255,128,128,128,128],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]];
    var x = 1;
    var distort_fact_prev = 0;
    var distort_fact = 0;
    iterations = 0;

    while (x > 0.0005)
    {
        iterations = iterations + 1;
        distort_fact_prev = distort_fact;
        container_cb = [];
        for (i = 0; i < Nc; i++) {
            container_cb[i] = [];
            for (j = 0; j < n; j++) {
                container_cb[i][j] = 0;
            }
        }

        count_container = [];
        for (i = 0; i < Nc; i++) {
            count_container[i] = 1;
        }


        
        indexClosestMatch = [];
        for (i = 0; i < Nb; i++) {

            index = ClosestMatch(tSet[i], codebook);  

            
            indexClosestMatch[i] = index;
            for (icc = 0; icc < n; icc++) {
                container_cb[index][icc] = container_cb[index][icc] + tSet[i][icc] ;
                
            }
            count_container[index] = count_container[index] + 1;
        }
           codebook=updateCodeBook(container_cb , codebook , count_container);
           distort_fact=distortion(codebook , indexClosestMatch , tSet ) ;
           x=Math.abs(distort_fact_prev - distort_fact);
           if (distort_fact_prev != 0) 
           {
                x=x/distort_fact_prev;
           }
           distort_fact_prev = distort_fact;
       }
       // hist_vq = histogram(indexClosestMatch, Nc);
       gambar_compressed = createCompressedImage(indexClosestMatch,codebook,A,p);
       var new_height = gambar_compressed.length;
        var new_width = gambar_compressed.length;
        var gambarBaruNew = [];

        var ctx = canvas.getContext("2d");
        for (i = 0; i < new_height - 2; i++) {
            gambarBaruNew[i] = [];
            for (j = 0; j < new_width - 2; j++) {
                gambarBaruNew[i][j] = gambar_compressed[i][j];
                ctx.fillStyle = 'rgb(' + gambarBaruNew[i][j] + ', ' + gambarBaruNew[i][j] + ', ' + gambarBaruNew[i][j] + ')';
                ctx.fillRect(i, j, 1, 1);
            }
        }
        imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        data = imageData.data;
       return canvas;

      

}


function createCompressedImage(indexClosestMatch,codebook,A,p)
{
    row = A.length;
    col = A[0].length;
    indexCM=0;
    var B = [];
    for (i = 0; i < row; i++) {
        B[i] = [];
        for (j = 0; j < col; j++) {
            B[i][j] = 0;
        }
    }
    for (i = 0; i < row; i += p) {
        for (j = 0; j < col; j += p) {
            index=indexClosestMatch[indexCM];
             l=0;
            for (k = 0; k < p; k++) {
                for (t = 0; t < p; t++) {
                     B[i+k][j+t] = codebook[index][l];
                     l = l + 1;
                }                
            }

            indexCM = indexCM + 1;
        }
    }
    return B;
}
function distortion(cb,indexClosestMatch,tset)
{
   sol=0;

    Nb = tset.length;
    n = tset[0].length;

    sum=0;

    for (i = 0; i < Nb; i++) {
         for (j = 0; j < n; j++) {
         
            sum = sum + Math.pow((cb[indexClosestMatch[i]][j] - tSet[i][j]),2);
         }
    }
    sol = sum/(Nb*n);

    return sol;

    
}

function updateCodeBook(container,cb,count)
{
    Nc = container.length;
    n = cb[0].length;

    for (i= 0;i< Nc;i++)
    {

        var temps = [];
        for (j= 0;j< n;j++)
        {
            if (container[i][j] == 0) 
            {
                temps[j] = cb[i][j];
            }
            else
            {
                temps[j] = Math.ceil(container[i][j] / count[i],0);
            }
            
        }
        
        cb[i] = temps;
       

    }
    return cb;
        
}
function ClosestMatch(TV, cb) {
    Nc = cb.length;
    n = cb[0].length;
    dist = Math.pow(2,32);

    index = 0;
    
    for (ic = 0; ic < Nc; ic++) {
        tempdist = 0;
        for (ic2 = 0; ic2 < n; ic2++) {
           tempdist = tempdist + Math.pow((TV[0, ic2] - cb[ic][ic2]),2);
        }

        if (dist > tempdist) {
            dist = tempdist;

            index = ic;
        }
        

    }

    return index;

}

function getTV(i, j, A, n, p) {
    var y = [];
    k = 0;
    y[0] = [];

    for (a = i; a <= p + i - 1; a++) {
        for (b = j; b <= j + p - 1; b++) {

            y[0][k] = A[a][b];
            k = k + 1;
        }
    }

    return y;

}

function msf(canvas)
{
    var c = canvas.getContext('2d');
    var col = canvas.height;
    var row = canvas.width;
    var size = col * row;
    var i, j, x, y;

    var vector = [];
    var inn = 0;
    for (i = 0; i < (col); i++) {
        vector[i] = [];
        for (j = 0; j < (row); j++) {
            vector[i][j] = c.getImageData(i, j, 1, 1).data[0];
            inn + 3;
        }
    }
    var A = vector;
     N = row * col;
            p = 12;
            n = p * p;
            Nb = N / n;

            vektor = [];
            for (i = 0; i < Nb; i++) {
                vektor[i] = [];
                for (j = 0; j < n; j++) {
                    vektor[i][j] = 0;
                }
            }

            
            idx = 0;
            for (i = 0; i < row; i += p) {
                for (j = 0; j < col; j += p) {
                    temp = vektorblok(i, j, A, n, p);

                    for (l = 0; l < n; l++) {

                        vektor[idx][l] = temp[0][l];
                    }

                    idx = idx + 1;
                }
            }
            var blok = [];
            fitur = [];
            for (var i = 0; i < vektor.length; i++) {
                // console.log(vektor[i]);
                vektorBaru = vektor[i];
                blokes = chunkArray(vektorBaru, 12);
                var mco = [];
                var ico = [];
                var trans = [];
                var sumtrans = [];
                
                for (var x = 0; x < blokes.length; x++) {
                    numbers = blokes[x];
                    co = [];
                    sum = 0;
                    for (var y = 0; y < numbers.length; y++) {
                        cox = Math.abs(numbers[y] - numbers[y+1]);
                        if (y==numbers.length-1) 
                        {
                            cox = Math.abs(numbers[y] - numbers[0]);
                        }
                        sum += cox;
                        co[y] = cox;
                    }

                    trs = [];
                    sumtrs = 0;
                    for (var y = 0; y < numbers.length; y++) {
                        coy = Math.abs(co[y]/sum);
                        trs[y] = coy;
                        sumtrs += coy;
                    }

                    sumtrans[x] = Math.round(sumtrs);
                    trans[x] = trs;
                    mco[x] =co;
                    ico[x] =co[x];
                }
                sumico =0;
                for (var x = 0; x < ico.length; x++) {
                    sumico +=ico[x];
                }
                var msf = [];
                for (var x = 0; x < ico.length; x++) {
                    msf[x] = ico[x]/sumico;
                }

                    fitur[i] = msf;
          
            }
            fiturMSF = concat(fitur);
            return fiturMSF;
    
}



function RGBdebug(canvas, board) {

    var c = canvas.getContext('2d');
    var height = canvas.height;
    var width = canvas.width;
    var i, j;
    var gambarBaru = [];
    var isigambar;
    
    var body = document.getElementById(board);
    var tbl = document.createElement("table");
    var tblBody = document.createElement("tbody");

    // creating all cells
    for (var i = 0; i < height; i++) {
    // creates a table row
    var row = document.createElement("tr");

    for (var j = 0; j < width; j++) {
    var cell = document.createElement("td");
    R = c.getImageData(i, j, 1, 1).data[0];
    G = c.getImageData(i, j, 1, 1).data[1];
    B = c.getImageData(i, j, 1, 1).data[2];
    var cellText = document.createTextNode("R : "+R+"\nG :"+G+"\nB :"+B);
    cell.appendChild(cellText);
    row.appendChild(cell);
    }

    tblBody.appendChild(row);
    }

    tbl.appendChild(tblBody);
    tbl.setAttribute("class", "table");
    body.appendChild(tbl);
}

function Lowpassdebug(canvas, tabel) {

    var c = canvas.getContext('2d');
    var height = canvas.height;
    var width = canvas.width;
    var i, j;
    var gambarBaru = [];
    var isigambar;
    
    var body = document.getElementById(tabel);
    var tbl = document.createElement("table");
    var tblBody = document.createElement("tbody");
    

    // creating all cells
    for (var i = 0; i < height; i++) {
    // creates a table row
    var row = document.createElement("tr");

    for (var j = 0; j < width; j++) {
    var cell = document.createElement("td");
    con = c.getImageData(i, j, 1, 1).data[0];
    var cellText = document.createTextNode(con);
    cell.appendChild(cellText);
    row.appendChild(cell);
    }

    tblBody.appendChild(row);
    }

    tbl.appendChild(tblBody);
    tbl.setAttribute("class", "table");
    body.appendChild(tbl);
}

function vqdebuger(canvas, tabel) {

    var c = canvas.getContext('2d');
    var height = canvas.height;
    var width = canvas.width;
    var i, j;
    var gambarBaru = [];
    var isigambar;
    
    var body = document.getElementById(tabel);
    var tbl = document.createElement("table");
    var tblBody = document.createElement("tbody");
    

    // creating all cells
    for (var i = 0; i < height; i++) {
    // creates a table row
    var row = document.createElement("tr");

    for (var j = 0; j < width; j++) {
    var cell = document.createElement("td");
    con = c.getImageData(i, j, 1, 1).data[0];
    var cellText = document.createTextNode(con);
    cell.appendChild(cellText);
    row.appendChild(cell);
    }

    tblBody.appendChild(row);
    }

    tbl.appendChild(tblBody);
    tbl.setAttribute("class", "table");
    body.appendChild(tbl);
}

function grayscaledebug(canvas, tabel) {

    var c = canvas.getContext('2d');
    var height = canvas.height;
    var width = canvas.width;
    var i, j;
    var gambarBaru = [];
    var isigambar;
    
    var body = document.getElementById(tabel);
    var tbl = document.createElement("table");
    var tblBody = document.createElement("tbody");
    

    // creating all cells
    for (var i = 0; i < height; i++) {
    // creates a table row
    var row = document.createElement("tr");

    for (var j = 0; j < width; j++) {
    var cell = document.createElement("td");
    con = c.getImageData(i, j, 1, 1).data[0];
    var cellText = document.createTextNode(con);
    cell.appendChild(cellText);
    row.appendChild(cell);
    }

    tblBody.appendChild(row);
    }

    tbl.appendChild(tblBody);
    tbl.setAttribute("class", "table");
    body.appendChild(tbl);
}



function debug(array, board)
{
    height = array.length;
    width = array[0].length;
    var body = document.getElementById(board);
    var tbl = document.createElement("table");
    var tblBody = document.createElement("tbody");
    

    // creating all cells
    var row = document.createElement("tr");
        var cell = document.createElement("td");
        var cellText = document.createTextNode(["NO","Nilai Histogram"]);
        cell.appendChild(cellText);
        row.appendChild(cell);
    for (var i = 0; i < height; i++) {
        // creates a table row
        var row = document.createElement("tr");

        for (var j = 0; j < width; j++) {
            var cell = document.createElement("td");
            con = array[i][j];
            var cellText = document.createTextNode(con);
            cell.appendChild(cellText);
            row.appendChild(cell);
        }

        tblBody.appendChild(row);
    }

    tbl.appendChild(tblBody);
    body.appendChild(tbl);
    tbl.setAttribute("class", "table");
}

function debug2(array)
{

    height = array.length;
    var body = document.getElementById("tabel");
    var tbl = document.createElement("table");
    var tblBody = document.createElement("tbody");
    

    // creating all cells
        var row = document.createElement("tr");
    for (var i = 0; i < height; i++) {      // creates a table row
        
            var cell = document.createElement("td");
            con = array[i];
            var cellText = document.createTextNode(con);
            cell.appendChild(cellText);
            row.appendChild(cell);
        

    }
        tblBody.appendChild(row);

    tbl.appendChild(tblBody);
    body.appendChild(tbl);
}

function msfDebug(array, board)
{
    height = array.length;
    width = array[0].length;
    var body = document.getElementById(board);
    var tbl = document.createElement("table");
    var tblBody = document.createElement("tbody");
    

    // creating all cells
    var row = document.createElement("tr");
        var cell = document.createElement("td");
        var cellText = document.createTextNode(["NO","Nilai Histogram"]);
        cell.appendChild(cellText);
        row.appendChild(cell);
    for (var i = 0; i < height; i++) {
        // creates a table row
        var row = document.createElement("tr");

        for (var j = 0; j < width; j++) {
            var cell = document.createElement("td");
            con = array[i][j];
            var cellText = document.createTextNode(con);
            cell.appendChild(cellText);
            row.appendChild(cell);
        }

        tblBody.appendChild(row);
    }

    tbl.appendChild(tblBody);
    body.appendChild(tbl);
    tbl.setAttribute("class", "table");
}
function concat(canvas)
        {
            var concater = [];

            for(var i = 0; i < canvas.length; i++)
            {
                concater = concater.concat(canvas[i]);
            }
            return concater;
        }

function chunkArray(myArray, chunk_size){
    var results = [];
    
    while (myArray.length) {
        results.push(myArray.splice(0, chunk_size));
    }
    
    return results;
}

function getSum(total, num) {
    return total + num;
}

function vektorblok(i, j, A, n, p) {
    var y = [];
    k = 0;
    y[0] = [];

    for (a = i; a <= p + i - 1; a++) {
        for (b = j; b <= j + p - 1; b++) {
            y[0][k] = A[a][b];
            k = k + 1;
        }
    }

    return y;

}


</script>
<!-- <script src="dist/fungsi.js"></script> -->
<script src="dist/jquery.min.js"></script>
<script src="dist/jquery.facedetection.js"></script> 
