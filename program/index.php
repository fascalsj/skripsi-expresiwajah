<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Implementasi Markov Stationary Feature - Vector Quantization pada Kasus Pengenalan Ekspresi Wajah</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="src/template/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="src/template/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="src/template/css/style.css">
  <style type="text/css">
    a:link, a:visited {
    /*background-color: #f44336;*/
    color: white;
    padding: 14px 25px;
    text-align: center; 
    text-decoration: none;
    display: inline-block;
}

a:hover, a:active {
    background-color: #b8c2d3;
}
  </style>
  <!-- endinject -->
  <!-- <link rel="shortcut icon" href="src/template/images/favicon.png" /> -->
</head>
<body>
  <div class="content-wrapper">
      <div class="row">
        <div class="col-12">
          10112317 - Fascal Sapty Jarockohir 
          <span class="d-flex align-items-center purchase-popup">
            <a> <h4>Implementasi Markov Stationary Feature - Vector Quantization pada Kasus Pengenalan Ekspresi Wajah </h4></a>
          </span><br>
            
        </div>
      </div>
      <div id="content" class="row">
        
      </div>
  </div>
  <script src="src/template/vendors/js/vendor.bundle.base.js"></script>
  <script src="src/template/vendors/js/vendor.bundle.addons.js"></script>

  <script src="src/template/js/off-canvas.js"></script>
  <script src="src/template/js/misc.js"></script>
  <script src="src/template/js/dashboard.js"></script>
  <script type="text/javascript">
      $("#content").load("home.php");
      function loadHalaman(e)
      {
        var jenis = $(e).attr("data-jenis")
         $("#content").load(jenis);
      }
  </script>
</body>

</html>
